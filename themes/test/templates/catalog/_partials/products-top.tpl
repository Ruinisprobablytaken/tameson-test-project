<div id="js-product-list-top" class="search-filter-nav products-selection clearfix d-flex justify-content-between">
  <div class="search-filter-item-count text-sm-center showing">
    {l s='%total% item(s)' d='Shop.Theme.Catalog' sprintf=[
    '%total%' => $listing.pagination.total_items
    ]}
  </div>
  <button class="search-filter-open" data-toggle="collapse" data-target="#slide-search_filters">
    <i class="material-icons">settings_input_component</i>
    {l s='Filters' d='Shop.Theme.Actions'} <span class="magnitude"></span>
  </button>
  {block name='sort_by'}
    {include file='catalog/_partials/sort-orders.tpl' sort_orders=$listing.sort_orders}
  {/block}
</div>
