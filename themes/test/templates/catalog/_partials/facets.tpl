<div id="slide-search_filters" class="slide-menu"> {* Initialize the menu *}
  <div class="controls clearfix"> {* Add controls *}
    <div class="col-xs-4">
      <button type="button" class="menu-close slide-menu-control" data-target="#slide-search_filters" data-toggle="collapse" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>

    <div class="col-xs-4">
      {block name='facets_title'}
        <p class="facets-title">{l s='Filter By' d='Shop.Theme.Actions'}</p>
      {/block}
    </div>

    <div class="col-xs-4">
      {block name='facets_clearall_button'}
        <div class="clear-all-wrapper slide-menu-control">
          <a href="{$clear_all_link}" data-search-url="{$clear_all_link}">
            {l s='Clear all' d='Shop.Theme.Actions'}
          </a>
        </div>
      {/block}
    </div>
  </div>
  <div class="filters-wrapper">
    <ul id="filters">
    </ul>
  </div>
  <div class="filters-footer">
    <button id="filter-submit" class="btn btn-primary" data-target="#slide-search_filters" data-toggle="collapse">Apply Filters <span class="magnitude"></span></button>
  </div>
</div>